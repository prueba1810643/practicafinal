# Repositorio de Practica Final 

## Autor
* Argueta Bravo Angel Jacob 
### Contacto
angeljacob938@gmail.com

## Instructor
* Mario Alberto Arredondo
### Contacto


## Descripcion

El archivo buscador.js utiliza  JavaScript y jQuery para realizar 
búsquedas de películas en The Movie Database. Cuando el usuario 
ingresa un término de búsqueda y hace clic en un botón, el código 
realiza una solicitud a la API de películas usando clave API y un 
token (credencial) de acceso. Los resultados de la búsqueda se 
presentan en una lista en el documento HTML. Cada elemento de la lista 
contiene la imagen de la película si es que está disponible, el 
título  y una breve descripción si es que esta disponible. 
