//Hecho por Argueta Bravo Angel Jacob
// 26/11/2023
$(document).ready(function () {
    var miLista = $("#miLista");
    var busqueda = $("#busqueda");
    $("#btn-buscar").on("click", function () {
        const palabra = busqueda.val();
        if (palabra.trim() === '') {
            alert('Por favor, ingrese un término de búsqueda');
            return;
        }
        // Clave de API y token (credencial) de acceso obtenidos en moviedb
        const apiKey = '62c0da045d43170ff4c8af929d8516e6';
        const accessToken = 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI2MmMwZGEwNDVkNDMxNzBmZjRjOGFmOTI5ZDg1MTZlNiIsInN1iI6IjY1NjE0YjVjMmIxMTNkMDBlYmE3YjFiNiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.kxNbzF49p7JXvmUbbZB-RkmKFfEyNvP-lh-zSE7rcYI';
        // Aqui se llama a la API de The Movie Database utilizando la clave api que nos proporciono  
        const apiUrl = `https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&query=${encodeURIComponent(palabra)}`;
        // Se configura el encabezado de una solicitud para autenticar la dicha solicitud hacie el servidor 
        const headers = {
            'Authorization': `Bearer ${accessToken}`
        };
        // Se realiza la solicitud a la API
        fetch(apiUrl, { headers })
        .then(response => response.json())
        .then(data => {
            // Se limpia la lista de resultados
            miLista.empty();
            // Se muestran los títulos de las películas en la lista
            data.results.forEach(movie => {
                // Se crea un elemento de lista para cada película
                const listItem = $('<li style="list-style-type: none;">');
                 // Se crea un contenedor para la imagen de la película
                const imagenContainer = $('<div style="float: left; margin-right: 10px;">');
                // Se muestra  la imagen de la película si está disponible
                if (movie.poster_path) {
                    const imagen = $(`<img src="https://image.tmdb.org/t/p/w200/${movie.poster_path}" alt="${movie.title}" class="movie-poster">`);
                    imagenContainer.append(imagen);
                }
                // Se crea un contenedor para la información de la película
                const infoContainer = $('<div>');
                // Se muestra el título de la película en negrita y más grande para diferenciarlo
                const titulo = $(`<p style="font-size: 18px; font-weight: bold; text-align: center;">${movie.title}</p>`);
                infoContainer.append(titulo);
                // Se muestra una breve descripción de la película
                if (movie.overview) {
                    const descripcion = $(`<p>${movie.overview}</p>`);
                    infoContainer.append(descripcion);
                }
                // Agregamos los contenedores al elemento de lista
                listItem.append(imagenContainer);
                listItem.append(infoContainer);
                // Agregamos una línea divisora para diferenciar las peliculas cuando 
                //no tienen imagen 
                const lineaDivisora = $('<hr style="clear: both;">');
                listItem.append(lineaDivisora);
                // Agregamos el elemento de lista a la lista principal
                miLista.append(listItem);
            });
        })
        .catch(error => {
            console.error('Error al realizar la búsqueda:', error);
        });
    });
});
